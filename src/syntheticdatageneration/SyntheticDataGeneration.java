/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntheticdatageneration;

import Documents.*;
import Patients.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import ratings.GenerateSyntheticRatings;

/**
 *
 * @author user
 */
public class SyntheticDataGeneration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /**
         * Documents
         */
       

        XML_Parser xml = new XML_Parser();

        // Full path of the ICD-10 ontology xml file 'ICD-10.xml'
        File icd10 = new File("ICD-10.xml");
        String icd10Path = icd10.getAbsolutePath();
        xml.parseXMLFile(icd10Path);

        System.out.println("Creating Documents ...");
        BuildDocuments bd = new BuildDocuments(xml);
        bd.setNumRated(70);
        bd.buildDocuments(10, 270);
        List<Documents> docs = bd.getNodes();
        bd.printDocuments();

        /**
         * Patients
         */
        System.out.println("Creating Patients ...");
        //Full path of the 'PatientCorePopulatedTable.txt' file
        File patient = new File("PatientCorePopulatedTable.txt");
        String patientPath = patient.getAbsolutePath();
        CreatePatient cp = new CreatePatient(patientPath);
        //Full path of the 'AdmissionsDiagnosesCorePopulatedTable.txt' file
        File admission = new File("AdmissionsDiagnosesCorePopulatedTable.txt");
        String admissionPath = admission.getAbsolutePath();
        cp.ReadIllnessFile(admissionPath);

        //The percentages for the three different groups
        double groupAper = 0.5;
        double groupBper = 0.3;
        double groupCper = 0.2;

        //The ranges from which a randomly number will be choosen as a patient's total number of ratings, according to his/her group
        int rangeAmin = 20;
        int rangeAmax = 100;

        int rangeBmin = 100;
        int rangeBmax = 250;

        int rangeCmin = 250;
        int rangeCmax = 500;

        cp.divideGroups(groupAper, rangeAmin, rangeAmax, groupBper, rangeBmin, rangeBmax, groupCper, rangeCmin, rangeCmax);

        //The percentages of the number of ratings that a user will give which will be relevant with his/her health problems
        double healthRelevant = 0.2;
        double noRelevant = 0.8;

        cp.setRatingsSpan(healthRelevant, noRelevant);
        int totalRatings = cp.getTotalRatings();
        /**
         * Ratings
         */
        System.out.println("Creating Ratings Data...");
        double tail = 0.5;
        GenerateSyntheticRatings rat = new GenerateSyntheticRatings(cp.getPatients(), docs, tail, bd.getCategoryLex());
        System.out.println("Finalize Rating Data");
        rat.setNumRating(totalRatings);

        //The percentages of the values of the ratings
        double one = 0.2, two = 0.1, three = 0.3, four = 0.2, five = 0.2;

        rat.generateRating(one, two, three, four, five);
        rat.printRatings();
    }

}
