/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documents;

import Patients.Patient;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import ratings.Ratings;

/**
 *
 * @author user
 */
public class BuildDocuments {

    private List<Documents> nodes;
    private String direc;
    private Set<String> stopwords;
    private Map<String, String> categoryLex;
    private Map<String, Set<String>> keywords;
    private int numRated;

    public BuildDocuments(XML_Parser xml) {
        categoryLex = xml.getCategoryLex();
        keywords = xml.getKeywords();
        nodes = new ArrayList<>();
        stopwords = new HashSet<>();
        getStopWords();
    }

    private void getStopWords() {
        try {
            File stop = new File("stopwords.txt");
            String stopPath = stop.getAbsolutePath();
            BufferedReader in = new BufferedReader(new FileReader(stopPath));
            String line;
            while ((line = in.readLine()) != null) {
                stopwords.add(line);
            }
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BuildDocuments.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BuildDocuments.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Documents> getNodes() {
        return nodes;
    }

    public Map<String, String> getCategoryLex() {
        return categoryLex;
    }

    private void buildKeywords(int num) {
        for (String s : keywords.keySet()) {
            Set<String> k = keywords.get(s);
            Documents d = new Documents(s, k, stopwords);
            d.setNumOfKeywords(num);
            nodes.add(d);
        }
    }

    public void setNumRated(int numRated) {
        this.numRated = numRated;
    }

    public int getNumRated() {
        return numRated;
    }

    private void buildDocs(int num) {
        int id = 0;
        for (Documents d : nodes) {
            id = d.createDocs(num, id, numRated);
        }
    }

    public void buildDocuments(int numOfKeywords, int numOfDocs) {
        buildKeywords(numOfKeywords);
        buildDocs(numOfDocs);
    }

    public void printDocuments() {
        try {
            PrintWriter writer = new PrintWriter("documents.txt", "UTF-8");
            writer.println("DOCUMENT_ID\t{KEYWORDS}");
            for (Documents d : nodes) {
                Map<Integer, Set<String>> docs = d.getDocs();
                for (int id : docs.keySet()) {
                    writer.print(id + "\t[ ");
                    Set<String> set = docs.get(id);
                    for (String s : set) {
                        writer.print(s + " ");
                    }
                    writer.print("]");
                    writer.println();
                }
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }
}
