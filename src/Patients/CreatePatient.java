/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Patients;

import Documents.BuildDocuments;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class CreatePatient {
    private Map<String, Patient> patients;
    private int totalRatings = 0;
    public CreatePatient(String file){
        patients = new HashMap<>();
        readPatientsFile(file);
    }
    
    private void readPatientsFile(String file){
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String line;
            line = in.readLine();
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if(line.equalsIgnoreCase("")){
                    continue;
                }
                String [] elements = line.split("\t");
                if(elements.length != 7){
                    System.out.println("> " + elements[0]);
                }
                
                Patient p;
                
                String id = elements[0];
                if(patients.containsKey(id)){
                    System.out.println("Error: The patient with id '" + id + "' already exists");
                    continue;
                }else{
                    p = new Patient(id);
                }
                p.setGender(elements[1]);
                p.setDob(elements[2]);
                p.setRace(elements[3]);
                p.setMaritalStat(elements[4]);
                p.setLang(elements[5]);
                p.setPoverty(elements[6]);
                patients.put(id, p);
            }
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreatePatient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreatePatient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void ReadIllnessFile(String file){
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String line;
            line = in.readLine();
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if(line.equalsIgnoreCase("")){
                    continue;
                }
                String [] elements = line.split("\t");
                if(elements.length != 4){
                    System.out.println("> " + elements[0]);
                }
                
                Patient p;
                
                String id = elements[0];
                if(patients.containsKey(id)){
                    p = patients.get(id);
                }else{
                    System.out.println("Could not find patient '" + id + "'");
                    continue;
                }
                p.addIllness(elements[2], elements[3]);
                
            }
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreatePatient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreatePatient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void divideGroups(double a, int rAmin, int rAmax, double b, int rBmin, int rBmax, double c, int rCmin, int rCmax){
        int totalUsers = patients.size();
        double gA = a * totalUsers;
        double gB = b * totalUsers;
        double gC = c * totalUsers;;
        int sizeA = (int) Math.floor(gA);
        int sizeB = (int) Math.floor(gB);
        int sizeC = (int) Math.floor(gC);
        System.out.println("sizeA: " + sizeA + " sizeB: " + sizeB + " sizeC: " + sizeC);
        List<Patient> pat = new ArrayList<>();
        pat.addAll(patients.values());
        int min = 0;
        int max = pat.size();
        int i = 0;
        while(i<sizeA){
            int ran = ThreadLocalRandom.current().nextInt(min, max);
            Patient p = pat.get(ran);
            if(!p.getGroup().equalsIgnoreCase("")){
                continue;
            }
            p.setGroup("A", rAmin, rAmax);
            totalRatings += p.getNumOfRatings();
            i++;
        }
        
        i = 0;
        while(i<sizeB){
            int ran = ThreadLocalRandom.current().nextInt(min, max);
            Patient p = pat.get(ran);
            if(!p.getGroup().equalsIgnoreCase("")){
                continue;
            }
            p.setGroup("B", rBmin, rBmax);
            totalRatings += p.getNumOfRatings();
            i++;
        }
        
        i = 0;
        while(i<sizeC){
            int ran = ThreadLocalRandom.current().nextInt(min, max);
            Patient p = pat.get(ran);
            if(!p.getGroup().equalsIgnoreCase("")){
                continue;
            }
            p.setGroup("C", rCmin, rCmax);
            totalRatings += p.getNumOfRatings();
            i++;
        }
    }
    
    public void setRatingsSpan(double in, double out){
        for(Patient p : patients.values()){
            int size = p.getNumOfRatings();
            int inS = (int) Math.floor(in*size);
            int outS = size - inS;
            int sum = inS + outS;
            if(sum < size){
                int dif = size - sum;
                outS += dif;
            }else if(sum > size){
                int dif = sum - size;
                outS -= dif;
            }
            p.setCategorySpan(inS, outS);
        }
    }

    public Map<String, Patient> getPatients() {
        return patients;
    }
    
    
    
    

    public int getTotalRatings() {
        return totalRatings;
    }
    
    
}
