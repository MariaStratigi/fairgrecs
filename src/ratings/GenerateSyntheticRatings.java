/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ratings;

import Documents.Documents;
import Patients.Patient;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author user
 */
public class GenerateSyntheticRatings {

    private Map<String, Patient> patients;
    private List<Documents> docs;
    private final double ratedPer;
    private int numRating;
    private Map<String, String> lex;
    private List<String> topCat;
    private List<Integer> head;

    public GenerateSyntheticRatings(Map<String, Patient> p, List<Documents> docs, double rat, Map<String, String> lex) {
        patients = p;
        this.docs = docs;
        this.ratedPer = rat / 100.0;
        System.out.println("ratedPer: " + ratedPer);
        this.lex = lex;
    }

    public void generateRating(double one, double two, double three, double four, double five) {
        int numPat = patients.size();
        int docNum = docs.size() * docs.get(0).getNumOfDocs();
        int mat = numPat * docNum;

        int numOne = (int) Math.floor(one * numRating);
        int numTwo = (int) Math.floor(two * numRating);
        int numThree = (int) Math.floor(three * numRating);
        int numFour = (int) Math.floor(four * numRating);
        int numFive = (int) Math.floor(five * numRating);
        int numTotal = numOne + numTwo + numThree + numFour + numFive;
        if (numTotal != numRating) {
            int dif = numRating - numTotal;
            if (dif < 0) {
                System.out.println("Hmmmm");
            }
            numFive += dif;
        }

        System.out.println("#Patients: " + numPat);
        System.out.println("#Categories: " + docs.size());
        System.out.println("#Docs: " + docNum);
        System.out.println("#Ratings: " + numRating);
        int flag = 0;
        int flagcount = 0;
        boolean phaseOne = true;
        int addOn = 10;
        while (numRating > 0) {
            int inC = 0;
            int outC = 0;
            flag = numRating;
            if (phaseOne) {
                for (Patient p : patients.values()) {
                    if (!p.hasMoreRatings()) {
                        continue;
                    }
                    String cat = "";
                    if (p.hasMoreInRatings()) {
                        inC++;
                        cat = getCategory(p, true);
                        Documents d = getDocument(cat);
                        if (p.addRating(d.getDocs(), d.getEndId())) {
                            p.adjustInCounter();
                            numRating--;
                        }
                    } else if (p.hasMoreOutRatings()) {
                        outC++;
                        cat = getCategory(p, false);
                        Documents d = getDocument(cat);
                        if (p.addRating(d.getDocs(), d.getEndId())) {
                            p.adjustOutCounter();
                            numRating--;
                        }
                    }
                }
            }
            if (flagcount > 0 && numRating != flag) {
                flagcount = 0;
            }
            
            if(!phaseOne){
                for (Patient p : patients.values()) {
                    if (!p.hasMoreRatings()) {
                        continue;
                    }
                    String cat = "";
                    if (p.hasMoreInRatings()) {
                        inC++;
                        cat = getCategory(p, true);
                        Documents d = getDocument(cat);
                        if (p.addRating(d.getDocs(), d.getEndId() + addOn)) {
                            p.adjustInCounter();
                            numRating--;
                        }
                    } else if (p.hasMoreOutRatings()) {
                        outC++;
                        cat = getCategory(p, false);
                        Documents d = getDocument(cat);
                        if (p.addRating(d.getDocs(), d.getEndId() + addOn)) {
                            p.adjustOutCounter();
                            numRating--;
                        }
                    }
                }
            }
            if (numRating == flag) {
                if(!phaseOne){
                    addOn += 10;
                }
                phaseOne = false;
            }
        }
        System.out.println("Done generating documents for rating");
        finalizeRatings(numOne, numTwo, numThree, numFour, numFive);
    }

    private void finalizeRatings(int numOne, int numTwo, int numThree, int numFour, int numFive) {
        int minP = 0;

        List<String> patientsIds = new ArrayList<>();
        patientsIds.addAll(patients.keySet());
        int numPat = patientsIds.size();
        int counter = 0;
        while (counter < numOne) {
            int randomNum = ThreadLocalRandom.current().nextInt(minP, numPat);
            String id = patientsIds.get(randomNum);
            Patient p = patients.get(id);
            if (p.FillRatings(1)) {
                counter++;
            }
        }
        counter = 0;
        while (counter < numTwo) {
            int randomNum = ThreadLocalRandom.current().nextInt(minP, numPat);
            String id = patientsIds.get(randomNum);
            Patient p = patients.get(id);
            if (p.FillRatings(2)) {
                counter++;
            }
        }
        counter = 0;
        while (counter < numThree) {
            int randomNum = ThreadLocalRandom.current().nextInt(minP, numPat);
            String id = patientsIds.get(randomNum);
            Patient p = patients.get(id);
            if (p.FillRatings(3)) {
                counter++;
            }
        }
        counter = 0;
        while (counter < numFour) {
            int randomNum = ThreadLocalRandom.current().nextInt(minP, numPat);
            String id = patientsIds.get(randomNum);
            Patient p = patients.get(id);
            if (p.FillRatings(4)) {
                counter++;
            }
        }
        counter = 0;
        while (counter < numFive) {
            int randomNum = ThreadLocalRandom.current().nextInt(minP, numPat);
            String id = patientsIds.get(randomNum);
            Patient p = patients.get(id);
            if (p.FillRatings(5)) {
                counter++;
            }
        }
    }

    private Documents getDocument(String name) {
        for (Documents d : docs) {
            if (d.getNodeName().equalsIgnoreCase(name)) {
                return d;
            }
        }
        return null;
    }

    private String getCategory(Patient p, boolean flag) {
        int min = 0;
        if (flag) {
            String str = p.getACategory();
            String s = lex.get(str);
            p.addRatingPerCat(s);
            return s;
        }
        int max = docs.size();

        String cat;
        while (true) {
            int ran = ThreadLocalRandom.current().nextInt(min, max);
            cat = docs.get(ran).getNodeName();
            if (!p.categoryExist(cat)) {
                break;
            }
        }
        p.addRatingPerCat(cat);
        return cat;
    }

    public void printRatings() {
        try {
            PrintWriter writer = new PrintWriter("ratings.txt", "UTF-8");
            writer.println("USER_ID\tDOCUMENT_ID\tRATE");
            for (Patient p : patients.values()) {
                Ratings r = p.getRatings();
                Map<Integer, Integer> rat = r.getRatings();
                for (int i : rat.keySet()) {
                    writer.println(p.getId() + "\t" + i + "\t" + rat.get(i));
                }
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }

    public void setNumRating(int sum) {
        this.numRating = sum;
    }


    public void setTopCat(List<String> topCat) {
        this.topCat = topCat;
    }

    public void createHead(int num) {
        head = new ArrayList<>();
        int j = 0;
        int size = topCat.size();
        for (int i = 0; i < num; i++) {
            if (j == size) {
                j = 0;
            }
            int id = getARandomDoc(topCat.get(j));
            head.add(id);
            j++;
        }
    }

    private int getARandomDoc(String cat) {
        Documents d = null;
        for (Documents doc : docs) {
            if (doc.getNodeName().equalsIgnoreCase(cat)) {
                d = doc;
                break;
            }
        }
        if (d == null) {
            System.out.println("Something went wrong with the category name");
            return -1;
        }
        Map<Integer, Set<String>> allDoc = d.getDocs();
        List<Integer> keys = new ArrayList<>();
        keys.addAll(allDoc.keySet());
        int min = 0;
        int max = keys.size();
        int ran = ThreadLocalRandom.current().nextInt(min, max);
        return keys.get(ran);
    }

    public void printRaingPerCat(){
        HashMap<String, Integer> rat = new HashMap<>();
        HashMap<String, Integer> us = new HashMap<>();
        for(Patient p : patients.values()){
            Map<String, Integer> r = p.getRatingPerCat();
            for(String s : r.keySet()){
                if(rat.containsKey(s)){
                    int count = rat.get(s);
                    count += r.get(s);
                    rat.put(s, count);
                }else{
                    rat.put(s, r.get(s));
                }
                if(us.containsKey(s)){
                    int count = us.get(s);
                    count++;
                    us.put(s,count);
                }else{
                    us.put(s,1);
                }
            }
        }
        rat = sortByValues(rat);
        us = sortByValues(us);
        try {
            PrintWriter writer = new PrintWriter("ratingsPerCat.txt", "UTF-8");
            for(String s : rat.keySet()){
                writer.println(s + "\t" + rat.get(s));
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
        try {
            PrintWriter writer = new PrintWriter("ratingsPerUser.txt", "UTF-8");
            for(String s : us.keySet()){
                writer.println(s + "\t" + us.get(s));
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }
    
    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
